# Additional Documentation

The following documentation might help:

- [GTK Book](https://gtk-rs.org/gtk4-rs/stable/latest/book/): General GTK-development for Rust.
- [presage](https://whisperfish.github.io/presage/presage/): The backend library used to interact with Signal.
- [GTK](https://docs.gtk.org/gtk4/): GTK reference, for C but also maps to Rust. You can also use the [Rust](https://gtk-rs.org/gtk4-rs/stable/latest/docs/gtk4/) documentation. 
- [Libadwaita](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/): Libadwaita reference, for C but also maps to Rust. You can also use the [Rust](https://world.pages.gitlab.gnome.org/Rust/libadwaita-rs/stable/latest/docs/libadwaita/index.html) documentation. 

