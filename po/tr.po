# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the flare package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: flare\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-02 13:37+0200\n"
"PO-Revision-Date: 2024-02-09 07:02+0000\n"
"Last-Translator: Sabri Ünal <libreajans@gmail.com>\n"
"Language-Team: Turkish <https://hosted.weblate.org/projects/schmiddi-on-"
"mobile/flare/tr/>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.4-dev\n"

#: data/resources/ui/about.blp:13
msgid "translator-credits"
msgstr ""
"Sabri Ünal <libreajans@gmail.com>\n"
"Oğuz Ersen <oguz@ersen.moe>"

#: data/resources/ui/channel_info_dialog.blp:50
#: data/resources/ui/setup_window.blp:369
msgid "Phone Number"
msgstr "Telefon Numarası"

#: data/resources/ui/channel_info_dialog.blp:66
msgid "Description"
msgstr "Açıklama"

#: data/resources/ui/channel_info_dialog.blp:76
msgid "Disappearing Messages"
msgstr "Kaybolan İletiler"

#: data/resources/ui/channel_info_dialog.blp:100
msgctxt "accessibility"
msgid "Reset Session"
msgstr "Oturumu Sıfırla"

#: data/resources/ui/channel_info_dialog.blp:118
msgid "Reset Session"
msgstr "Oturumu Sıfırla"

#: data/resources/ui/channel_info_dialog.blp:128
#, fuzzy
msgctxt "accessibility"
msgid "Clear Messages"
msgstr "İletileri Temizle"

#: data/resources/ui/channel_info_dialog.blp:149
msgid "Clear Messages"
msgstr "İletileri Temizle"

#: data/resources/ui/channel_list.blp:10
#: data/resources/ui/channel_messages.blp:15
msgid "Offline"
msgstr "Çevrimdışı"

#: data/resources/ui/channel_list.blp:20
msgid "Search"
msgstr "Ara"

#: data/resources/ui/channel_list.blp:23 data/resources/ui/window.blp:61
msgctxt "accessibility"
msgid "Search"
msgstr "Ara"

#: data/resources/ui/channel_messages.blp:21
msgid "No Channel Selected"
msgstr "Kanal Seçilmedi"

#: data/resources/ui/channel_messages.blp:22
msgid "Select a channel"
msgstr "Kanal seç"

#: data/resources/ui/channel_messages.blp:181
msgctxt "accessibility"
msgid "Remove the reply"
msgstr "Cevabı kaldır"

#: data/resources/ui/channel_messages.blp:184
msgctxt "tooltip"
msgid "Remove reply"
msgstr "Cevabı kaldır"

#: data/resources/ui/channel_messages.blp:204
msgctxt "accessibility"
msgid "Remove an attachment"
msgstr "Eki kaldır"

#: data/resources/ui/channel_messages.blp:207
msgctxt "tooltip"
msgid "Remove attachment"
msgstr "Eki kaldır"

#: data/resources/ui/channel_messages.blp:228
msgctxt "accessibility"
msgid "Add an attachment"
msgstr "Ek ekle"

#: data/resources/ui/channel_messages.blp:236
msgctxt "tooltip"
msgid "Add attachment"
msgstr "Ek ekle"

#: data/resources/ui/channel_messages.blp:252
msgctxt "accessibility"
msgid "Message input"
msgstr "ileti girdisi"

#: data/resources/ui/channel_messages.blp:258
msgctxt "tooltip"
msgid "Message input"
msgstr "ileti girdisi"

#: data/resources/ui/channel_messages.blp:270
msgctxt "accessibility"
msgid "Send"
msgstr "Gönder"

#: data/resources/ui/channel_messages.blp:274
msgctxt "tooltip"
msgid "Send"
msgstr "Gönder"

#: data/resources/ui/dialog_clear_messages.blp:5
#: data/resources/ui/window.blp:166
msgid "Clear messages"
msgstr "İletileri temizle"

#: data/resources/ui/dialog_clear_messages.blp:6
msgid ""
"This will clear all locally stored messages. This will also close the "
"application."
msgstr ""
"Bu, yerel olarak depolanan tüm iletileri temizleyecek. Aynı zamanda "
"uygulamayı da kapatacak."

#: data/resources/ui/dialog_clear_messages.blp:11
#: data/resources/ui/dialog_unlink.blp:11
#: data/resources/ui/linked_devices_window.blp:50 src/gui/window.rs:309
msgid "Cancel"
msgstr "İptal"

#: data/resources/ui/dialog_clear_messages.blp:12
msgid "Clear"
msgstr "Temizle"

#: data/resources/ui/dialog_unlink.blp:5 data/resources/ui/window.blp:155
msgid "Unlink"
msgstr "Bağı Kaldır"

#: data/resources/ui/dialog_unlink.blp:6
msgid ""
"This will unlink this device and remove all locally saved data. This will "
"also close the application such that it can be relinked when opening it "
"again."
msgstr ""
"Bu işlem aygıtınızın bağını kaldıracak ve yerel olarak depolanan tüm "
"verileri kaldıracak. Bu işlem aynı zamanda uygulamayı tekrar açarken yeniden "
"bağlanabilecek şekilde kapatacak."

#: data/resources/ui/dialog_unlink.blp:12
msgid "Unlink and delete all messages"
msgstr "Bağı kaldır ve tüm iletileri sil"

#: data/resources/ui/dialog_unlink.blp:13
msgid "Unlink but keep messages"
msgstr "Bağı kaldır ama iletileri sakla"

#: data/resources/ui/error_dialog.blp:5
msgid "Error"
msgstr "hata"

#: data/resources/ui/error_dialog.blp:26
msgid "Please consider reporting this error."
msgstr "Lütfen hatayı bildirin."

#: data/resources/ui/error_dialog.blp:43 data/resources/ui/setup_window.blp:478
#: data/resources/ui/submit_captcha_dialog.blp:28
msgid "Close"
msgstr "Kapat"

#: data/resources/ui/error_dialog.blp:44
msgid "Report"
msgstr "Raporla"

#: data/resources/ui/linked_devices_window.blp:13
#: data/resources/ui/window.blp:160
msgid "Linked Devices"
msgstr "Ağlı Aygıtlar"

#: data/resources/ui/linked_devices_window.blp:31
msgid "Add Linked Device"
msgstr "Bağlı Aygıt Ekle"

#: data/resources/ui/linked_devices_window.blp:32
msgid "Please insert the URL of the device to link."
msgstr "Lütfen bağlantı verilecek aygıtın URL'sini girin."

#: data/resources/ui/linked_devices_window.blp:42
msgid "Device URL"
msgstr "Cihaz URL'si"

#: data/resources/ui/linked_devices_window.blp:51
msgid "Add Device"
msgstr "Aygıt Ekle"

#: data/resources/ui/message_item.blp:13
msgid "Reply"
msgstr "Cevapla"

#: data/resources/ui/message_item.blp:20
msgid "Delete"
msgstr "Sil"

#: data/resources/ui/message_item.blp:28
msgid "Copy"
msgstr "Kopyala"

#: data/resources/ui/message_item.blp:37
msgid "Save as…"
msgstr "Farklı Kaydet…"

#: data/resources/ui/message_item.blp:45
msgid "Open with…"
msgstr "Birlikte Aç…"

#: data/resources/ui/new_channel_dialog.blp:14
#, fuzzy
msgid "New Chat"
msgstr "Sohbet"

#: data/resources/ui/new_channel_dialog.blp:20 data/resources/ui/window.blp:49
msgctxt "accessibility"
msgid "Menu"
msgstr "Menü"

#: data/resources/ui/new_channel_dialog.blp:35
#, fuzzy
msgid "Search Contacts"
msgstr "Kişileri Eşzamanla"

#: data/resources/ui/new_channel_dialog.blp:71
msgid "List Contacts By:"
msgstr ""

#: data/resources/ui/new_channel_dialog.blp:73
msgid "First Name"
msgstr ""

#: data/resources/ui/new_channel_dialog.blp:78
msgid "Surname"
msgstr ""

#: data/resources/ui/preferences_window.blp:6
msgid "General"
msgstr "Genel"

#: data/resources/ui/preferences_window.blp:10
msgid "Automatically Download Attachments"
msgstr "Ekleri Kendiliğinden İndir"

#: data/resources/ui/preferences_window.blp:11
msgid "Attachment types selected below will be automatically downloaded"
msgstr "Aşağıda seçilen ek türleri kendiliğinden indirilecek"

#: data/resources/ui/preferences_window.blp:14
msgid "Images"
msgstr "Görüntüler"

#: data/resources/ui/preferences_window.blp:18
msgid "Videos"
msgstr "Videolar"

#: data/resources/ui/preferences_window.blp:22
msgid "Files"
msgstr "Dosyalar"

#: data/resources/ui/preferences_window.blp:26
msgid "Voice Messages"
msgstr "Sesli İletiler"

#: data/resources/ui/preferences_window.blp:31
msgid "Notifications"
msgstr "Bildirimler"

#: data/resources/ui/preferences_window.blp:32
msgid "Notifications for new messages"
msgstr "Yeni iletiler için bildirimler"

#: data/resources/ui/preferences_window.blp:35
msgid "Send Notifications"
msgstr "Bildirim Gönder"

#: data/resources/ui/preferences_window.blp:39
msgid "Send Notifications on Reactions"
msgstr "Tepkiler Hakkında Bildirim Gönder"

#: data/resources/ui/preferences_window.blp:44
msgid "Background Notifications"
msgstr "Arka Plan Bildirimleri"

#: data/resources/ui/preferences_window.blp:45
msgid "Fetch notifications while the app is closed"
msgstr "Uygulama kapalıyken bildirimleri al"

#: data/resources/ui/preferences_window.blp:52
msgid "Mobile Compatibility"
msgstr "Mobil Uyumluluk"

#: data/resources/ui/preferences_window.blp:53
msgid ""
"These options may want to be changed for a better experience on touch- and "
"mobile devices. The default values of those preferences are chosen to be "
"useful on desktop devices."
msgstr ""
"Dokunmatik ve mobil aygıtlarda daha iyi deneyim için bu seçenekler "
"değiştirilebilir. Bu tercihlerin öntanımlı değerleri, masaüstü aygıtlarda "
"kullanışlı olacak şekilde seçilmiştir."

#: data/resources/ui/preferences_window.blp:56
msgid "Selectable Message Text"
msgstr "Seçilebilir İleti Metni"

#: data/resources/ui/preferences_window.blp:57
msgid "Selectable text can interfere with swipe-gestures on touch-screens"
msgstr ""
"Seçilebilir metin, dokunmatik ekranlarda kaydırma hareketlerine engel "
"olabilir"

#: data/resources/ui/preferences_window.blp:61
msgid "Press “Enter” to Send Message"
msgstr "İletiyi göndermek için “Enter” tuşuna basın"

#: data/resources/ui/preferences_window.blp:62
msgid ""
"It may not be possible to send messages with newlines on touch keyboards not "
"allowing “Control + Enter”"
msgstr ""
"“Control + Enter” kullanımına izin vermeyen dokunmatik klavyelerde yeni "
"satırlı mesaj göndermek mümkün olmayabilir"

#: data/resources/ui/setup_window.blp:17 data/resources/ui/setup_window.blp:24
msgid "Welcome to Flare"
msgstr "Flare'e Hoş Geldiniz"

#: data/resources/ui/setup_window.blp:41
msgid "Set Up Flare"
msgstr "Flare'ı Kur"

#. Translators: Flare name in metainfo.
#: data/resources/ui/setup_window.blp:50
#: data/de.schmidhuberj.Flare.desktop.in.in:5
#: data/de.schmidhuberj.Flare.metainfo.xml.in:8
msgid "Flare"
msgstr "Flare"

#: data/resources/ui/setup_window.blp:53
msgid ""
"Flare is an unofficial Signal client. Note that Flare is not stable and "
"there are bugs to be expected. If you are experiencing any bugs, consult the "
"<a href=\"https://gitlab.com/schmiddi-on-mobile/flare/-/issues\">issue "
"tracker</a> and open issues if you are experiencing a new issue. Also note "
"that due to being a third-party application, Flare cannot guarantee the same "
"level of security and privacy as official Signal applications do. If you or "
"someone you need to contact requires a strong level of security, do not use "
"Flare. Consult the <a href=\"https://gitlab.com/schmiddi-on-mobile/flare/-/"
"blob/master/README.md#security\">README</a> for more information."
msgstr ""

#: data/resources/ui/setup_window.blp:62 data/resources/ui/setup_window.blp:69
msgid "Setup Primary Device or Link Device?"
msgstr "Birincil Aygıt ya da Bağlantı Aygıt Kurulsun mu?"

#: data/resources/ui/setup_window.blp:85 data/resources/ui/setup_window.blp:317
#: data/resources/ui/setup_window.blp:324
msgid "Primary Device"
msgstr "Birincil Aygıt"

#: data/resources/ui/setup_window.blp:98 data/resources/ui/setup_window.blp:153
#: data/resources/ui/setup_window.blp:160
#: data/resources/ui/setup_window.blp:177
msgid "Link Device"
msgstr "Aygıt Bağla"

#: data/resources/ui/setup_window.blp:105
msgid "Primary Device or Link Device?"
msgstr "Birincil Aygıt mı Bağlantı Aygıt mı?"

#: data/resources/ui/setup_window.blp:117
msgid "You can use Flare either as a primary or secondary device."
msgstr "Flare birincil ya da ikincil aygıt olarak kullanılabilir."

#: data/resources/ui/setup_window.blp:126
msgid "Linked Device (Recommended)"
msgstr "Bağlı Aygıt (Önerilen)"

#: data/resources/ui/setup_window.blp:131
msgid ""
"Flare can be an application linked to your primary device, similar to the "
"official Signal Desktop. This mode has been more extensively tested and "
"mostly works."
msgstr ""

#: data/resources/ui/setup_window.blp:140
msgid "Primary Device (Disabled)"
msgstr "Birincil Aygıt (Devre Dışı)"

#: data/resources/ui/setup_window.blp:145
msgid ""
"Flare can act as a primary device, similar to the official Signal "
"applications on Android or IOS. This mode is currently not supported by "
"Flare, even though initial tests seem to indicate that it works. If you are "
"willing to test it, you may enable the button below (and if you don't know "
"how to do that, you should not think about testing primary-device support)."
msgstr ""

#: data/resources/ui/setup_window.blp:194
msgid ""
"For linking as a secondary device, Flare will need a device name that will "
"be shown in the official application."
msgstr ""

#: data/resources/ui/setup_window.blp:204
msgid "Device Name"
msgstr "Aygıt Adı"

#: data/resources/ui/setup_window.blp:208
#: data/resources/ui/setup_window.blp:377
msgid "Developer Options"
msgstr "Geliştirici Seçenekleri"

#: data/resources/ui/setup_window.blp:211
#: data/resources/ui/setup_window.blp:380
msgid "Signal Servers"
msgstr "Signal Sunucuları"

#: data/resources/ui/setup_window.blp:217
msgid ""
"The next page will show a QR code which has to be scanned from the primary "
"device. Note that you only have a timeframe of about one minute to scan the "
"code, so please already prepare your official application for scanning the "
"QR code."
msgstr ""

#: data/resources/ui/setup_window.blp:225
#: data/resources/ui/setup_window.blp:237
#: data/resources/ui/setup_window.blp:291
msgid "Link device"
msgstr "Aygıt bağla"

#: data/resources/ui/setup_window.blp:242
msgid "Scan Code"
msgstr "Kod Tara"

#: data/resources/ui/setup_window.blp:243
msgid "Scan this code with another Signal app logged into your account."
msgstr ""
"Bu kodu, hesabınıza giriş yapmış başka bir Signal uygulamasıyla tarayın."

#: data/resources/ui/setup_window.blp:270
msgid "Link without scanning"
msgstr "Taramadan bağla"

#: data/resources/ui/setup_window.blp:280
msgid "Manual linking"
msgstr "Elle bağlama"

#: data/resources/ui/setup_window.blp:296
msgid "Manual Linking"
msgstr "Elle Bağlama"

#: data/resources/ui/setup_window.blp:297
msgid ""
"If your device can't scan the QR code, you can manually enter the link "
"provided here"
msgstr ""
"Aygıtınız QR kodunu tarayamıyorsa, sağlanan bağlantıyı burada elle "
"girebilirsiniz"

#: data/resources/ui/setup_window.blp:308
msgid "Copy to clipboard"
msgstr "Panoya kopyala"

#: data/resources/ui/setup_window.blp:341
msgid "Continue"
msgstr "Devam Et"

#: data/resources/ui/setup_window.blp:358
msgid ""
"For registering as a primary device, Flare will need your phone number and a "
"completion of the captcha. Please give the phone number in international "
"E.164-format, including the leading \"+\" and country code. The captcha can "
"be completed <a href=\"https://signalcaptchas.org/registration/generate."
"html\">here</a>, afterwards copy the \"Open Signal\" link and paste it into "
"the below field. Note that this captcha is only valid for about one minute, "
"so please proceed fast."
msgstr ""

#: data/resources/ui/setup_window.blp:373
#: data/resources/ui/submit_captcha_dialog.blp:20
msgid "Captcha"
msgstr "Captcha"

#: data/resources/ui/setup_window.blp:386
msgid ""
"After submitting this information, you will get a SMS from Signal with a "
"verification code. Insert this verification code on the next page."
msgstr ""

#: data/resources/ui/setup_window.blp:395
#: data/resources/ui/setup_window.blp:403
#: data/resources/ui/setup_window.blp:420
#: data/resources/ui/setup_window.blp:445
msgid "Confirm"
msgstr "Onayla"

#: data/resources/ui/setup_window.blp:436
msgid "Insert the verification code you received from Signal here."
msgstr "Signal'den aldığınız doğrulama kodunu buraya girin."

#: data/resources/ui/setup_window.blp:453
#: data/resources/ui/setup_window.blp:461
msgid "Finished"
msgstr "Bitti"

#: data/resources/ui/setup_window.blp:494
msgid ""
"Flare is almost ready, it is waiting for contacts to finish syncing. This "
"may take a few seconds, you can start to use Flare once the contacts are "
"synced. A few final notes before you start using Flare:"
msgstr ""
"Flare neredeyse hazır, kişilerin eşzamanlamasının bitmesi bekleniyor. Bu "
"işlem birkaç saniye sürebilir, kişiler eşzamanlandıktan sonra Flare "
"kullanmaya başlayabilirsiniz. Flare kullanmaya başlamadan önce son birkaç "
"not:"

#: data/resources/ui/setup_window.blp:503
msgid "Known Bugs"
msgstr "Bilinen Hatalar"

#: data/resources/ui/setup_window.blp:508
msgid ""
"Make sure to not send any messages while the messages are still received by "
"Flare, this makes sure that all required information is received. We are "
"trying to fix this problem. Furthermore, sending to contacts you have not "
"yet received messages from can lead to rate limiting. Make sure to receive a "
"message from a contact before you send too many. For a full list of bugs, "
"visit the <a href=\"https://gitlab.com/schmiddi-on-mobile/flare/-/issues/?"
"label_name%5B%5D=bug\">issue tracker</a>."
msgstr ""

#: data/resources/ui/setup_window.blp:518
msgid "Contact"
msgstr "İletişim"

#: data/resources/ui/setup_window.blp:523
msgid ""
"Want to leave some feedback about Flare, or just talk a little bit? Our <a "
"href=\"https://matrix.to/#/#flare-signal:matrix.org\">Matrix room</a> is "
"open for everyone."
msgstr ""

#: data/resources/ui/shortcuts.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Genel"

#: data/resources/ui/shortcuts.blp:14
msgctxt "shortcut window"
msgid "Show shortcuts"
msgstr "Kısayolları göster"

#: data/resources/ui/shortcuts.blp:19
msgctxt "shortcut window"
msgid "Go to settings"
msgstr "Ayarlara git"

#: data/resources/ui/shortcuts.blp:24
msgctxt "shortcut window"
msgid "Go to about page"
msgstr "Hakkında sayfasına git"

#: data/resources/ui/shortcuts.blp:29
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Menüyü aç"

#: data/resources/ui/shortcuts.blp:34
msgctxt "shortcut window"
msgid "Close window"
msgstr "Pencereyi kapat"

#: data/resources/ui/shortcuts.blp:40
msgctxt "shortcut window"
msgid "Channels"
msgstr "Kanallar"

#: data/resources/ui/shortcuts.blp:43
msgctxt "shortcut window"
msgid "Go to channel 1…9"
msgstr "1…9 arası kanala git"

#: data/resources/ui/shortcuts.blp:48
msgctxt "shortcut window"
msgid "Search in channels"
msgstr "Kanallarda ara"

#: data/resources/ui/shortcuts.blp:53
msgctxt "shortcut window"
msgid "Upload attachment"
msgstr "Ek yükle"

#: data/resources/ui/shortcuts.blp:58
msgctxt "shortcut window"
msgid "Focus the text input box"
msgstr "Metin girdi kutusuna odaklan"

#: data/resources/ui/shortcuts.blp:63
msgctxt "shortcut window"
msgid "Load more messages"
msgstr "Daha çok ileti yükle"

#: data/resources/ui/submit_captcha_dialog.blp:5
#: data/resources/ui/window.blp:171
msgid "Submit Captcha"
msgstr "Captcha Gönder"

#. Translators: Do not translate tags around "on this website".
#: data/resources/ui/submit_captcha_dialog.blp:7
msgid ""
"Submit a Captcha challenge. The token can be obtained from the error message "
"that was displayed from Flare. The captcha must be filled out <a "
"href=\"https://signalcaptchas.org/challenge/generate.html\">on this website</"
"a> and the link to open Signal must be pasted to the corresponding entry. "
"Note that the captcha is only valid for about one minute."
msgstr ""
"Bir Captcha sınaması gönderin. Jeton Flare'den görüntülenen hata mesajından "
"elde edilebilir. Captcha <a href=\"https://signalcaptchas.org/challenge/"
"generate.html\">bu web sitesinde</a> doldurulmalı ve Signal'i açma "
"bağlantısı ilgili girdiye yapıştırılmalıdır. Captcha'nın yalnızca bir dakika "
"kadar geçerli olduğunu unutmayın."

#: data/resources/ui/submit_captcha_dialog.blp:16
msgid "Token"
msgstr "Jeton"

#: data/resources/ui/submit_captcha_dialog.blp:29
msgid "Submit"
msgstr "Gönder"

#: data/resources/ui/text_entry.blp:13
msgid "Message"
msgstr "ileti"

#: data/resources/ui/text_entry.blp:40
msgctxt "accessibility"
msgid "Insert an emoji"
msgstr "Emoji ekle"

#: data/resources/ui/text_entry.blp:48
msgctxt "tooltip"
msgid "Insert emoji"
msgstr "Emoji ekle"

#: data/resources/ui/window.blp:25
msgid "Channel list"
msgstr "Kanal listesi"

#: data/resources/ui/window.blp:38
msgctxt "accessibility"
msgid "Add Conversation"
msgstr "Konuşma Ekle"

#: data/resources/ui/window.blp:79
msgid "Chat"
msgstr "Sohbet"

#: data/resources/ui/window.blp:119 src/gui/channel_item.rs:59
msgid "is typing"
msgstr ""

#: data/resources/ui/window.blp:150
msgid "Settings"
msgstr "Ayarlar"

#: data/resources/ui/window.blp:176
msgid "Synchronize Contacts"
msgstr "Kişileri Eşzamanla"

#: data/resources/ui/window.blp:181
msgid "Keybindings"
msgstr "Kısayollar"

#: data/resources/ui/window.blp:186
msgid "About"
msgstr "Hakkında"

#: data/resources/ui/window.blp:191
msgid "Quit"
msgstr "Çık"

#: src/backend/channel.rs:537 src/backend/channel.rs:679
msgid "Note to self"
msgstr "Kendime not"

#: src/backend/contact.rs:193 src/backend/contact.rs:275
msgid "Unknown contact"
msgstr ""

#: src/backend/manager.rs:442
msgid "You"
msgstr "Siz"

#: src/backend/message/call_message.rs:100
msgid "Incoming call"
msgstr "Gelen arama"

#: src/backend/message/call_message.rs:101
msgid "Outgoing call"
msgstr "Giden arama"

#: src/backend/message/call_message.rs:102
msgid "Call started"
msgstr "arama başladı"

#: src/backend/message/call_message.rs:103
msgid "Call ended"
msgstr "Arama bitti"

#: src/backend/message/call_message.rs:104
msgid "Call declined"
msgstr "arama reddedildi"

#: src/backend/message/call_message.rs:105
msgid "Unanswered call"
msgstr "Cevapsız çağrı"

#: src/backend/message/reaction_message.rs:72
msgid "{sender} reacted {emoji} to a message."
msgstr "{sender}, bir iletiye {emoji} tepkisini verdi."

#: src/backend/message/reaction_message.rs:80
msgid "Reacted {emoji} to a message."
msgstr "Bir iletiye {emoji} tepkisini verdi."

#: src/backend/message/text_message.rs:381
msgid "Sent an image"
msgid_plural "Sent {} images"
msgstr[0] "Görüntü gönderildi"
msgstr[1] "{} görüntü gönderildi"

#: src/backend/message/text_message.rs:383
msgid "Sent an video"
msgid_plural "Sent {} videos"
msgstr[0] "Video gönderildi"
msgstr[1] "{} video gönderildi"

#: src/backend/message/text_message.rs:386
msgid "Sent a voice message"
msgid_plural "Sent {} voice messages"
msgstr[0] "Sesli ileti gönderildi"
msgstr[1] "{} sesli ileti gönderildi"

#: src/backend/message/text_message.rs:391
msgid "Sent a file"
msgid_plural "Sent {} files"
msgstr[0] "Dosya gönderildi"
msgstr[1] "{} dosya gönderildi"

#: src/error.rs:91
msgid "I/O Error."
msgstr "G/Ç hatası."

#: src/error.rs:96
msgid "There does not seem to be a connection to the internet available."
msgstr "İnternet bağlantısı yok görünüyor."

#: src/error.rs:101
msgid "Something glib-related failed."
msgstr "Glib ile ilgili bir şey başarısız oldu."

#: src/error.rs:106
msgid "The communication with Libsecret failed."
msgstr "Libsecret ile iletişim başarısız oldu."

#: src/error.rs:111
msgid ""
"The backend database failed. Please restart the application or delete the "
"database and relink the application."
msgstr ""
"Arka uç veritabanı başarısız oldu. Lütfen uygulamayı yeniden başlatın veya "
"veritabanını silin ve uygulamayı yeniden bağlayın."

#: src/error.rs:116
msgid ""
"You do not seem to be authorized with Signal. Please delete the database and "
"relink the application."
msgstr ""
"Signal ile yetkilendirilmemişsiniz görünüyor. Lütfen veritabanını silip "
"uygulamayı yeniden bağlayın."

#: src/error.rs:121
msgid "Sending a message failed."
msgstr "İleti gönderilemedi."

#: src/error.rs:126
msgid "Receiving a message failed."
msgstr "İleti alınamadı."

#: src/error.rs:131
msgid ""
"Something unexpected happened with the signal backend. Please retry later."
msgstr ""
"Signal arka ucunda beklenmedik bir hata oluştu. Lütfen daha sonra tekrar "
"deneyin."

#: src/error.rs:136
msgid "The application seems to be misconfigured."
msgstr "Uygulama yanlış yapılandırılmış görünüyor."

#: src/error.rs:141
msgid "A part of the application crashed."
msgstr "Uygulamanın bir bölümü çöktü."

#: src/error.rs:151
msgid "Please check your internet connection."
msgstr "Lütfen internet bağlantınızı denetleyin."

#: src/error.rs:156
msgid "Please delete the database and relink the device."
msgstr "Lütfen veritabanını silip aygıtı yeniden bağlayın."

#: src/error.rs:163
msgid "The database path at {} is no folder."
msgstr "{} adresindeki veritabanı yolu klasör değil."

#: src/error.rs:168
msgid "Please restart the application with logging and report this issue."
msgstr ""
"Lütfen uygulamayı günlük kaydı ile yeniden başlatın ve bu sorunu bildirin."

#: src/gui/channel_info_dialog.rs:197
msgid "Never"
msgstr "Asla"

#: src/gui/channel_item.rs:63
msgid "Draft"
msgstr ""

#: src/gui/preferences_window.rs:25
msgid "Watch for new messages while closed"
msgstr "Kapalıyken yeni iletileri izle"

#: src/gui/preferences_window.rs:99
msgid "Background permission"
msgstr "Arka plan izni"

#: src/gui/preferences_window.rs:100
msgid "Use settings to remove permissions"
msgstr "İzinleri kaldırmak için ayarları kullanın"

#: src/gui/setup_window.rs:34
msgctxt "Signal Server"
msgid "Production"
msgstr "Canlı"

#: src/gui/setup_window.rs:38
msgctxt "Signal Server"
msgid "Staging"
msgstr "Hazırlık"

#: src/gui/setup_window.rs:129
msgid "Copied to clipboard"
msgstr "Panoya kopyalandı"

#. How to format time. Should probably be %H:%M (meaning print hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:102 src/gui/utility.rs:126
msgid "%H:%M"
msgstr "%H:%M"

#. How to format a date with time. Should probably be similar to %Y-%m-%d %H:%M (meaning print year, month from 01-12, day from 01-31 (each separated by -), hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:105
msgid "%Y-%m-%d %H:%M"
msgstr "%Y-%m-%d %H:%M"

#: src/gui/utility.rs:138
msgid "Today"
msgstr "Bugün"

#: src/gui/utility.rs:141
msgid "Yesterday"
msgstr "Dün"

#. How to format a human-readable date including the year. Should probably be similar to %Y-%m-%d (meaning print year, month from 01-12, day from 01-31 (each separated by -)).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:147
msgid "%Y-%m-%d"
msgstr "%Y-%m-%d"

#. How to format a human-readable date excluding the year. Should probably be similar to "%a., %d. %b" (meaning print abbreviated weekday, day from 1-31 and abbreviated month name).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:151
msgid "%a., %e. %b"
msgstr "%a., %e. %b"

#: src/gui/utility.rs:159
msgid "Attachment"
msgstr "Ek"

#: src/gui/window.rs:304 src/gui/window.rs:310
msgid "Remove Messages"
msgstr "İletiler Kaldır"

#: src/gui/window.rs:305
msgid "This will remove all locally stored messages from this channel"
msgstr "Bu, yerel olarak depolanan tüm iletileri bu kanaldan kaldıracak"

#. Translators: Flare summary in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:6
#: data/de.schmidhuberj.Flare.metainfo.xml.in:10
msgid "Chat with your friends on Signal"
msgstr "Signal üstünden arkadaşlarınla sohbet et"

#: data/de.schmidhuberj.Flare.desktop.in.in:15
msgid "messaging;chat;signal;"
msgstr "mesajlaşma;iletişim;sohbet;signal;"

#: data/de.schmidhuberj.Flare.metainfo.xml.in:13
msgid "Julian Schmidhuber"
msgstr ""

#. Translators: Description of Flare in metainfo.
#: data/de.schmidhuberj.Flare.metainfo.xml.in:29
#, fuzzy
msgid ""
"Flare is an unofficial app for Signal. It is still in development and "
"doesn't include all the features that the official Signal apps do. More "
"information can be found on its feature roadmap."
msgstr ""
"Flare, Linux'tan Signal kullanan arkadaşlarınızla sohbet etmenizi sağlayan "
"resmi olmayan bir uygulamadır. Halen geliştirme aşamasındadır ve resmi "
"Signal uygulamaları kadar çok özellik içermez."

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml.in:33
msgid ""
"Please note that using this application will probably worsen your security "
"compared to using official Signal applications. Use with care when handling "
"sensitive data. Look at the projects README for more information about "
"security."
msgstr ""
"Lütfen bu uygulamayı kullanmanın, resmi Signal uygulamalarını kullanmaya "
"kıyasla güvenliğinizi muhtemelen daha da kötüleştireceğini unutmayın. Hassas "
"verileri işlerken dikkatli kullanın. Güvenlik hakkında daha fazla bilgi için "
"projenin README dosyasına bakın."

#: data/de.schmidhuberj.Flare.metainfo.xml.in:600
msgid "Overview of the application"
msgstr "Uygulamaya genel bakış"

#: data/de.schmidhuberj.Flare.gschema.xml:6
msgid "Window width"
msgstr "Pencere genişliği"

#: data/de.schmidhuberj.Flare.gschema.xml:10
msgid "Window height"
msgstr "Pencere yüksekliği"

#: data/de.schmidhuberj.Flare.gschema.xml:14
msgid "Window maximized state"
msgstr "Pencerenin ekranı kaplama durumu"

#: data/de.schmidhuberj.Flare.gschema.xml:19
msgid ""
"The device name when linking Flare. This requires the application to be re-"
"linked."
msgstr ""
"Flare bağlanırken aygıt adı. Bu, uygulamanın yeniden bağlanmasını gerektirir."

#: data/de.schmidhuberj.Flare.gschema.xml:24
msgid "Automatically download images"
msgstr "Görüntüleri kendiliğinden indir"

#: data/de.schmidhuberj.Flare.gschema.xml:28
msgid "Automatically download videos"
msgstr "Videoları kendiliğinden indir"

#: data/de.schmidhuberj.Flare.gschema.xml:32
msgid "Automatically download voice messages"
msgstr "Sesli iletileri kendiliğinden indir"

#: data/de.schmidhuberj.Flare.gschema.xml:36
msgid "Automatically download files"
msgstr "Dosyaları kendiliğinden indir"

#: data/de.schmidhuberj.Flare.gschema.xml:41
msgid "Send notifications"
msgstr "Bildirim gönder"

#: data/de.schmidhuberj.Flare.gschema.xml:45
msgid "Run in background when closed"
msgstr "Kapatıldığında arka planda çalış"

#: data/de.schmidhuberj.Flare.gschema.xml:49
msgid "Send notifications when retrieving reactions"
msgstr "Tepkileri alırken bildirim gönder"

#: data/de.schmidhuberj.Flare.gschema.xml:54
msgid "Can messages be selected"
msgstr "Mesajlar seçilebilir mi?"

#: data/de.schmidhuberj.Flare.gschema.xml:58
msgid "Send a message when the Enter-key is pressed"
msgstr "Enter tuşuna basıldığında ileti gönder"

#: data/de.schmidhuberj.Flare.gschema.xml:63
msgid "How to sort contacts, e.g with \"firstname\" or \"surname\""
msgstr ""

#~ msgid "Channel Information"
#~ msgstr "Kanal Bilgileri"

#~ msgid "Linking device"
#~ msgstr "Aygıt bağlanıyor"

#~ msgid "Sending messages"
#~ msgstr "İletiler gönderiliyor"

#~ msgid "Receiving messages"
#~ msgstr "İletiler alınıyor"

#~ msgid "Replying to a message"
#~ msgstr "İleti yanıtlama"

#~ msgid "Reacting to a message"
#~ msgstr "İletiye tepki verme"

#~ msgid "Attachments"
#~ msgstr "Ekler"

#~ msgid "Encrypted storage"
#~ msgstr "Şifreli depolama"

#~ msgid "Notifications, optionally in the background"
#~ msgstr "İsteğe bağlı olarak arka planda bildirimler"

#~ msgid ""
#~ "More features are planned, blocked, or not-planned. Consult the README "
#~ "for more information about them."
#~ msgstr ""
#~ "Daha fazla özellik planlanmış, engellenmiş veya planlanmamıştır. Bunlar "
#~ "hakkında daha fazla bilgi için README'ye bakın."

#~ msgid "Dowload images automatically"
#~ msgstr "Görüntüleri kendiliğinden indir"

#~ msgid "Dowload videos automatically"
#~ msgstr "Videoları kendiliğinden indir"

#~ msgid "Dowload files automatically"
#~ msgstr "Dosyaları kendiliğinden indir"

#~ msgid "Dowload voice messages automatically"
#~ msgstr "Sesli iletileri kendiliğinden indir"

#~ msgid "Linking"
#~ msgstr "Bağlama"

#~ msgid "The device will need to be relinked to take effect."
#~ msgstr "Etkinleşmesi için aygıtın yeniden bağlanması gerekir."

#~ msgid "Started calling."
#~ msgstr "Arama başlatıldı."

#~ msgid "Hung up."
#~ msgstr "Kapatıldı."

#~ msgid "Is busy."
#~ msgstr "meşgul."

#~ msgid "Send"
#~ msgstr "Gönder"

#~ msgid "Load media"
#~ msgstr "Ortam yükle"

#, fuzzy
#~ msgid "Signal GTK Client"
#~ msgstr "Resmi olmayan bir Signal GTK istemcisi"

#, fuzzy
#~ msgid "Changed"
#~ msgstr "Kanallar"

#, fuzzy
#~ msgid "Notifications on reactions received (configurable)."
#~ msgstr "Bildirim gönder"

#, fuzzy
#~ msgid "Button to scroll down in the messages view."
#~ msgstr "Görüntüleri kendiliğinden indir"

#, fuzzy
#~ msgid "Keyboard shortcut Ctrl+Q to close window."
#~ msgstr "Kısayolları göster"

#, fuzzy
#~ msgid "Message deletion."
#~ msgstr "İleti Depolama"

#, fuzzy
#~ msgid "Updated application icon and emblem."
#~ msgstr "Uygulamanın bir bölümü çöktü."

#, fuzzy
#~ msgid "Copy action in message menu."
#~ msgstr "İletiyi yanıtla"

#, fuzzy
#~ msgid "Unlink without deleting messages."
#~ msgstr "Bağı kaldır ama iletileri sakla"

#, fuzzy
#~ msgid "Messages and notifications for calls."
#~ msgstr "Bildirim gönder"

#, fuzzy
#~ msgid "Greatly refactored messages."
#~ msgstr "İletileri temizle"

#, fuzzy
#~ msgid "Changed:"
#~ msgstr "Kanallar"

#, fuzzy
#~ msgid "UI improvements for the channel list."
#~ msgstr "Kanallarda ara"

#, fuzzy
#~ msgid "Notification support."
#~ msgstr "Bildirimler"

#, fuzzy
#~ msgid "New message storage backend."
#~ msgstr "İleti Depolama"

#, fuzzy
#~ msgid "Group storage."
#~ msgstr "Şifreli depolama"

#, fuzzy
#~ msgid "Search for the channel view."
#~ msgstr "Kanallarda ara"

#, fuzzy
#~ msgid "Message storage."
#~ msgstr "İleti Depolama"

#, fuzzy
#~ msgid "Keyboard shortcuts."
#~ msgstr "Kısayolları göster"

#, fuzzy
#~ msgid "Fix message receiving, sending attachments."
#~ msgstr "Ek gönderme ve alma"

#~ msgid "Load image"
#~ msgstr "Görüntü yükle"

#~ msgid "Load video"
#~ msgstr "Video yükle"

#~ msgid "Load file"
#~ msgstr "Dosya yükle"

#, fuzzy
#~ msgid "Load voice message"
#~ msgstr "Daha çok ileti yükle"

#~ msgid "Please scan this QR code with your primary device."
#~ msgstr "Lütfen bu QR kodunu birincil aygıtınızla tarayın."

#~ msgid "Please wait until the contacts are displayed after linking."
#~ msgstr "Lütfen bağ kurulduktan sonra kişiler görüntülenene kadar bekleyin."

#~ msgid "An unofficial Signal GTK client"
#~ msgstr "Resmi olmayan bir Signal GTK istemcisi"

#~ msgid ""
#~ "An unofficial GTK client for the messaging application Signal. This is a "
#~ "very simple application with many features missing compared to the "
#~ "official applications."
#~ msgstr ""
#~ "Mesajlaşma uygulaması Signal için resmi olmayan bir GTK istemcisi. Bu, "
#~ "resmi uygulamalara kıyasla birçok özelliği eksik olan çok basit bir "
#~ "uygulamadır."

#~ msgid "Other"
#~ msgstr "Diğer"

#~ msgctxt "accessibility"
#~ msgid "Download"
#~ msgstr "İndir"

#~ msgid "React"
#~ msgstr "Tepki ver"

#~ msgctxt "accessibility"
#~ msgid "Reset Identity"
#~ msgstr "Kimliği Sıfırla"

#~ msgid "Reset Identity"
#~ msgstr "Kimliği Sıfırla"

#~ msgid "Message Storage"
#~ msgstr "İleti Depolama"

#~ msgid "Number of messages to load on startup"
#~ msgstr "Başlangıçta yüklenecek ileti sayısı"

#~ msgid "Per channel"
#~ msgstr "Kanal başına"

#~ msgid "Number of messages to load on request"
#~ msgstr "İstek halinde yüklenecek ileti sayısı"

#, fuzzy
#~ msgid "The amount of messages to load on startup for each channel"
#~ msgstr "Başlangıçta yüklenecek ileti sayısı"

#, fuzzy
#~ msgid "The amount of messages to load on request for each channel"
#~ msgstr "İstek halinde yüklenecek ileti sayısı"

#~ msgid ""
#~ "Notifications for new messages. These notifications will only work while "
#~ "Flare is open, if you also want to be notified when this application is "
#~ "closed, consider using something like messenger-notify."
#~ msgstr ""
#~ "Yeni iletiler için bildirimler. Bu bildirimler yalnızca Flare açıkken "
#~ "çalışacaktır, bu uygulama kapatıldığında da bildirim almak istiyorsanız "
#~ "messenger-notify gibi bir şey kullanmayı düşünün."

#~ msgid "Are you sure?"
#~ msgstr "Emin misin?"
